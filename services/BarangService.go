package services

import (
	"belajargin/db"
	"belajargin/db/dbmodels"
	"belajargin/hosts/stringbuilder"
	"belajargin/models"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"log"
)

// BarangService untuk mendeklarasikan Fungsi Service
type BarangService struct {
	General models.GeneralModel
}

func (service *BarangService) GetAllBarangs() ([]dbmodels.Barang, error) {

	sugarLogger := service.General.OttoZapLog
	sugarLogger.Info("BarangService: GetAllBarang",
		zap.Any("req", ""))
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "BarangService: GetAllBarang")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}

	reqmid := stringbuilder.GenMidRequest{
		Latitude:  "89098098",
		Longitude: " 7987987987",
		Dati2:     234,
	}

	mid, err := stringbuilder.GenMid(reqmid)

	log.Println("mnid == ", mid)
	res, err := Db.GetAllBarang()

	if err != nil {
		// todo log
		return res, err
	}

	return res, nil
}

func (service *BarangService) GetBarangById(id int64) ([]dbmodels.Barang, error) {

	sugarLogger := service.General.OttoZapLog
	sugarLogger.Info("BarangService: GetBarang",
		zap.Int64("Id", id))
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "BarangService: GetBarang")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}

	reqmid := stringbuilder.GenMidRequest{
		Latitude:  "89098098",
		Longitude: " 7987987987",
		Dati2:     234,
	}

	mid, err := stringbuilder.GenMid(reqmid)

	log.Println("mnid == ", mid)
	res, err := Db.GetBarangById(id)

	if err != nil {
		// todo log
		return res, err
	}

	return res, nil
}

func (service *BarangService) AddNewBarang(usr dbmodels.Barang) (string, error) {
	sugarlogger := service.General.OttoZapLog
	sugarlogger.Info("UserService : Add New User")
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "User Service : Add New User")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}
	res, err := Db.AddNewBarang(usr)

	if err != nil {
		return res, err
	}
	return res, nil
}

func (service *BarangService) UpdateBarang(id int64, usr dbmodels.Barang) (string, error) {
	sugarlogger := service.General.OttoZapLog
	sugarlogger.Info("BarangService : Update Barang")
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "Barang Service : Update Barang")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}
	res, err := Db.UpdateBarang(usr, id)
	if err != nil {
		return res, err
	}
	return res, nil
}

func (service *BarangService) DeleteBarang(id int64) (string, error) {
	sugarlogger := service.General.OttoZapLog
	sugarlogger.Info("BarangService : Delete Barang")
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "Barang Service")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}
	res, err := Db.DeleteBarang(id)

	if err != nil {
		return res, err
	}
	return res, nil
}
