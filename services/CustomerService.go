package services

import (
	"belajargin/db"
	"belajargin/db/dbmodels"
	"belajargin/hosts/stringbuilder"
	"belajargin/models"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"log"
)

type CustomerService struct {
	General models.GeneralModel
}

func (service *CustomerService) GetAllCustomer() ([]dbmodels.Customer, error) {

	sugarLogger := service.General.OttoZapLog
	sugarLogger.Info("CustomerService: GetAllCustomer",
		zap.Any("req", ""))
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "CustomerService: GetAllCustomer")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}

	reqmid := stringbuilder.GenMidRequest{
		Latitude:  "89098098",
		Longitude: " 7987987987",
		Dati2:     234,
	}

	mid, err := stringbuilder.GenMid(reqmid)

	log.Println("mnid == ", mid)
	res, err := Db.GetAllCustomer()

	if err != nil {
		// todo log
		return res, err
	}

	return res, nil
}

func (service *CustomerService) GetCustomerById(id int64) ([]dbmodels.Customer, error) {

	sugarLogger := service.General.OttoZapLog
	sugarLogger.Info("BarangService: GetCustomer",
		zap.Int64("Id", id))
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "CustomerService: GetCustomerById")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}

	reqmid := stringbuilder.GenMidRequest{
		Latitude:  "89098098",
		Longitude: " 7987987987",
		Dati2:     234,
	}

	mid, err := stringbuilder.GenMid(reqmid)

	log.Println("mnid == ", mid)
	res, err := Db.GetCustomerById(id)

	if err != nil {
		// todo log
		return res, err
	}

	return res, nil
}

func (service *CustomerService) AddNewCustomer(usr dbmodels.Customer) (string, error) {
	sugarlogger := service.General.OttoZapLog
	sugarlogger.Info("CustomerService : Add New Customer")
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "Costumer Service : Add New Customerr")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}
	res, err := Db.AddNewCustomer(usr)

	if err != nil {
		return res, err
	}
	return res, nil
}

func (service *CustomerService) UpdateCustomer(id int64, usr dbmodels.Customer) (string, error) {
	sugarlogger := service.General.OttoZapLog
	sugarlogger.Info("CustomerService : Update Customer")
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "Customer Service : Update Customer")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}
	log.Println("id")
	log.Println(id)
	res, err := Db.UpdateCustomer(usr, id)
	log.Println("id di services : ", res)
	if err != nil {
		return res, err
	}
	return res, nil
}

func (service *CustomerService) DeleteCustomer(id int64) (string, error) {
	sugarlogger := service.General.OttoZapLog
	sugarlogger.Info("CustomerService : Delete Customer")
	span, _ := opentracing.StartSpanFromContext(service.General.Context, "Customer Service")
	defer span.Finish()

	Db := db.DbPostgres{
		General: service.General,
	}
	res, err := Db.DeleteCustomer(id)

	if err != nil {
		return res, err
	}
	return res, nil
}
