package controllers

import (
	"belajargin/db/dbmodels"
	"belajargin/models"
	"belajargin/services"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	ottologger "ottodigital.id/library/logger"

	ottodigitalutils "ottodigital.id/library/utils"
)

type BarangController struct{}

func (controller *BarangController) GetAllBarangs(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestBarang

	sugarLogger := ottologger.GetLogger()
	ctrlName := "BarangController"
	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}
	span := TracingEmptyFirstControllerCtx(ctx, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var BarangsService = new(services.BarangService)
	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	BarangsService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	res, err := BarangsService.GetAllBarangs()

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}
	result.Data = res
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}

func (controller *BarangController) GetBarangById(ctx *gin.Context) {

	var result models.ResponseModel
	var request models.RequestBarang

	sugarLogger := ottologger.GetLogger()
	ctrlName := "BarangController"

	ids := ctx.Param("id")
	id ,_ := strconv.ParseInt(ids, 10, 32)

	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var BarangService = new(services.BarangService)

	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	BarangService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	res, err := BarangService.GetBarangById(id)

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}
	result.Data = res
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)

}

func (controller *BarangController) AddNewBarang(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestBarang

	sugarLogger := ottologger.GetLogger()
	ctrlName := "BarangController"
	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var BarangService = new(services.BarangService)

	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:",
		zap.String("SPANID", spanID),
		zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	BarangService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	var res dbmodels.Barang
	res.NamaBarang = request.NamaBarang
	res.HargaBarang = request.HargaBarang
	res.Detail = request.Detail

	rezult, err := BarangService.AddNewBarang(res)

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:",
			zap.String("SPANID", spanID),
			zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}

	result.Data = rezult
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:",
		zap.String("SPANID", spanID),
		zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}

func (controller *BarangController) UpdateBarang(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestBarang
	sugarLogger := ottologger.GetLogger()
	ctrlName := "BarangController"

	ids := ctx.Param("id")

	id ,_ := strconv.ParseInt(ids, 10, 32)

	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var BarangService = new(services.BarangService)

	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	BarangService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	var res dbmodels.Barang
	res.NamaBarang = request.NamaBarang
	res.HargaBarang = request.HargaBarang
	res.Detail = request.Detail

	rezult, err := BarangService.UpdateBarang(id, res)

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}
	result.Data = rezult
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}

func (controller *BarangController) DeleteBarang(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestBarang

	sugarLogger := ottologger.GetLogger()
	ctrlName := "UserController"

	ids := ctx.Param("id")
	id ,_ := strconv.ParseInt(ids, 10, 32)

	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var BarangService = new(services.BarangService)
	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	BarangService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}
	res, err := BarangService.DeleteBarang(id)
	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}
	result.Data = res
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}
