package controllers

import (
	"belajargin/db/dbmodels"
	"belajargin/models"
	"belajargin/services"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	ottologger "ottodigital.id/library/logger"

	ottodigitalutils "ottodigital.id/library/utils"
)

type CustomerController struct{}

func (controller *CustomerController) GetAllCustomer(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestCustomer

	sugarLogger := ottologger.GetLogger()
	ctrlName := "CustomerController"
	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}
	span := TracingEmptyFirstControllerCtx(ctx, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var CustomerService = new(services.CustomerService)
	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	CustomerService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	res, err := CustomerService.GetAllCustomer()

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}
	result.Data = res
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}

func (controller *CustomerController) GetCustomerById(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestCustomer

	sugarLogger := ottologger.GetLogger()
	ctrlName := "CustomerController"

	ids := ctx.Param("id")
	id, _ := strconv.ParseInt(ids, 10, 32)

	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var CustomerService = new(services.CustomerService)

	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	CustomerService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	res, err := CustomerService.GetCustomerById(id)
	log.Println(err)
	if err != nil {
		log.Println("masuk error")
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
		return
	}

	if len(res) == 0 {
		result.Data = res
		result.ResponseCode = "00"
		result.Message = "id tidak ada"

		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Any("result", result))

		ctx.JSON(http.StatusOK, result)
		return
	}

	result.Data = res
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)

}

func (controller *CustomerController) AddNewCustomer(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestCustomer

	sugarLogger := ottologger.GetLogger()
	ctrlName := "CustomerController"
	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var CustomerService = new(services.CustomerService)

	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:",
		zap.String("SPANID", spanID),
		zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	CustomerService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	var res dbmodels.Customer
	res.CustomerName = request.CustomerName
	res.Address = request.Address
	res.Username = request.Username
	res.Password = request.Password

	rezult, err := CustomerService.AddNewCustomer(res)

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:",
			zap.String("SPANID", spanID),
			zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}

	result.Data = rezult
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:",
		zap.String("SPANID", spanID),
		zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}

func (controller *CustomerController) UpdateCustomer(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestCustomer
	sugarLogger := ottologger.GetLogger()
	ctrlName := "CustomerController"

	ids := ctx.Param("id")
	id, _ := strconv.ParseInt(ids, 10, 32)
	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var CustomerService = new(services.CustomerService)

	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	CustomerService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}

	var res dbmodels.Customer
	res.CustomerName = request.CustomerName
	res.Address = request.Address
	res.Username = request.Username
	res.Password = request.Password

	x, err := CustomerService.GetCustomerById(id)

	if len(x) == 0 {
		result.ResponseCode = "01"
		result.Message = "ID Doesnt exists"
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
		return
	}

	rezult, err := CustomerService.UpdateCustomer(id, res)

	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
		return
	}

	result.Data = rezult
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}

func (controller *CustomerController) DeleteCustomer(ctx *gin.Context) {
	var result models.ResponseModel
	var request models.RequestCustomer

	sugarLogger := ottologger.GetLogger()
	ctrlName := "CustomerController"

	ids := ctx.Param("id")
	id, _ := strconv.ParseInt(ids, 10, 32)

	if err := ctx.ShouldBindJSON(&request); err != nil {
		go sugarLogger.Error("Body Request Error", zap.Error(err))
		return
	}

	span := TracingFirstControllerCtx(ctx, request, ctrlName)
	c := ctx.Request.Context()
	context := opentracing.ContextWithSpan(c, span)

	defer span.Finish()

	var CustomerService = new(services.CustomerService)
	spanID := ottodigitalutils.GetSpanId(span)
	sugarLogger.Info("REQUEST:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("BODY", request),
		zap.Any("HEADER", ctx.Request.Header))

	CustomerService.General = models.GeneralModel{
		ParentSpan: span,
		OttoZapLog: sugarLogger,
		SpanId:     spanID,
		Context:    context,
	}
	res, err := CustomerService.DeleteCustomer(id)
	if err != nil {
		result.ResponseCode = "01"
		result.Message = err.Error()
		sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
			zap.Error(err))
		ctx.JSON(http.StatusOK, result)
	}
	result.Data = res
	result.ResponseCode = "00"
	result.Message = "success"

	sugarLogger.Info("RESPONSE:", zap.String("SPANID", spanID), zap.String("CTRL", ctrlName),
		zap.Any("result", result))

	ctx.JSON(http.StatusOK, result)
}
