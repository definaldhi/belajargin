package models

// RequestBarang ... buat request produk
type RequestBarang struct {
	ID          int64  `json:"id"`
	NamaBarang  string `json:"nama_barang"`
	HargaBarang string `json:"harga_barang"`
	Detail      string `json:"detail"`
}

// RequestCustomer ... untuk request data customer
type RequestCustomer struct {
	ID           int64  `json:"id"`
	CustomerName string `json:"customer_name"`
	Address      string `json:"address"`
	Username     string `json:"username"`
	Password     string `json:"password"`
}
