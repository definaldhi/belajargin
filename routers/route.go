package routers

import (
	"belajargin/controllers"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"go.uber.org/zap"
	"io"
	"os"
	"ottodigital.id/library/httpserver/ginserver"
	ottologger "ottodigital.id/library/logger"
	"ottodigital.id/library/ottotracing"
	"ottodigital.id/library/utils"
)

var (
	serviceName string

	debugMode string
	readTo    int
	writeTo   int
)

func init() {

	serviceName = utils.GetEnv("SERVICE_NAME", "belajargin-SERVICE")

	debugMode = utils.GetEnv("DEBUG_MODE", "debug")

}

type OttoRouter struct {
	Tracer   opentracing.Tracer
	Reporter jaeger.Reporter
	Closer   io.Closer
	Err      error
	GinFunc  gin.HandlerFunc
	Router   *gin.Engine
}

// Server ...
func Server(listenAddr string) error {

	fmt.Println("listenAddr -> ", listenAddr)

	ottoRouter := OttoRouter{}
	ottoRouter.InitTracing()
	ottoRouter.Routers()
	defer ottoRouter.Close()

	err := ginserver.GinServerUp(listenAddr, ottoRouter.Router)

	if err != nil {
		fmt.Println("Error:", err)

		return err
	}
	sugarLogger := ottologger.GetLogger()
	sugarLogger.Info("Server UP ", zap.String("Address:", listenAddr))

	return err

}

func (ottoRouter *OttoRouter) Routers() {

	gin.SetMode(debugMode)

	router := gin.New()

	router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "OPTIONS", "DELETE", "PUT"},
		AllowHeaders:     []string{"Origin", "authorization", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		//AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge: 86400,
	}))

	BarangController := new(controllers.BarangController)
	CustomerController := new(controllers.CustomerController)

	router.Use(ottoRouter.GinFunc)
	router.Use(gin.Recovery())

	// routes
	qris := router.Group("/ottopoint")
	{
		vZeroOneZero := qris.Group("/v0.1.0")
		{
			barang := vZeroOneZero.Group("/barang") // bisadiganti
			{
				//merchant.POST("/csv",UploadMerchantController.Csv)
				barang.GET("/", BarangController.GetAllBarangs)
				barang.POST("/", BarangController.AddNewBarang)
				barang.DELETE("/:id", BarangController.DeleteBarang)
				barang.PUT("/:id", BarangController.UpdateBarang)
				barang.GET("/:id", BarangController.GetBarangById)

			}

			//vZeroOneZero.POST("/check-version", v1.NfcCheckVersion)
		}
	}

	qris = router.Group("/ottopoint")
	{
		vZeroOneZero := qris.Group("/v0.1.0")
		{
			customer := vZeroOneZero.Group("/customer") // bisadiganti
			{
				//merchant.POST("/csv",UploadMerchantController.Csv)
				customer.GET("/", CustomerController.GetAllCustomer)
				customer.POST("/", CustomerController.AddNewCustomer)
				customer.DELETE("/:id", CustomerController.DeleteCustomer)
				customer.GET("/:id", CustomerController.GetCustomerById)
				customer.PUT("/:id", CustomerController.UpdateCustomer)

			}
		}
	}
	ottoRouter.Router = router

}

func (ottoRouter *OttoRouter) InitTracing() {

	hostName, err := os.Hostname()
	if err != nil {
		hostName = "PROD"
	}

	tracer, reporter, closer, err := ottotracing.InitTracing(fmt.Sprintf("%s::%s", serviceName, hostName), "13.250.21.165:5775", ottotracing.WithEnableInfoLog(true))
	if err != nil {
		fmt.Println("Error:", err)
	}
	opentracing.SetGlobalTracer(tracer)

	ottoRouter.Closer = closer
	ottoRouter.Reporter = reporter
	ottoRouter.Tracer = tracer
	ottoRouter.Err = err
	ottoRouter.GinFunc = ottotracing.OpenTracer([]byte("api-request-"))

}

func (ottoRouter *OttoRouter) Close() {

	ottoRouter.Closer.Close()
	ottoRouter.Reporter.Close()

}
