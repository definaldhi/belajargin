package stringbuilder

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/kelseyhightower/envconfig"
	"belajargin/hosts"
)

type EnvStringBuilder struct {
	Host 				string	`envconfig:"HOSTADDRES" default:"http://13.228.25.85:8995"`
	EndpointGenMpan		string 	`envconfig:"ENDPOINT_GEN_MPAN" default:"/merchant/gen_pan/ottopay"`
	EndpointGenMid		string 	`envconfig:"ENDPOINT_GEN_MID" default:"/merchant/gen_mid"`
}

var (
	envStringBuilder EnvStringBuilder
)

func init()  {
	err := envconfig.Process("STRINGBUILDER", &envStringBuilder)
	if err != nil {
		fmt.Println("Failed to get STRINGBUILDER env:", err)
	}
}

func GenMpan() (*GenMpanResponse, error)  {
	var resp GenMpanResponse

	url := envStringBuilder.Host + envStringBuilder.EndpointGenMpan

	data, err := hosts.HTTPGet(url, nil)
	if err != nil {
		logs.Error("generate mpan ", err.Error())
		return &resp, err
	}

	err = json.Unmarshal(data, &resp)
	if err != nil {
		logs.Error("Failed to unmarshaling response GenMpan from StringBuilder ", err.Error())
		return &resp, err
	}

	return &resp, err
}

func GenMid(request GenMidRequest) (*GenMidResponse, error)  {

	var resp GenMidResponse

	url := envStringBuilder.Host + envStringBuilder.EndpointGenMid

	data, err := hosts.HTTPPost(url, request)

	if err != nil {
		logs.Error("generate mid ", err.Error())
		return &resp, err
	}

	err = json.Unmarshal(data, &resp)
	if err != nil {
		logs.Error("Failed to unmarshaling response GenMID from StringBuilder ", err.Error())
		return &resp, err
	}

	return &resp, err

}