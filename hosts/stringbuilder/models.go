package stringbuilder


type GenMpanResponse struct {
	Pan 	string `json:"PAN"`
}

type GenMidRequest struct {
	AgentID      string `json:"agent_id"`
	Latitude     string `json:"latitude"`
	Longitude    string `json:"longitude"`
	Provinsi     int64    `json:"provinsi"`
	Dati2        int64    `json:"dati2"`
	Kecamatan    int64    `json:"kecamatan"`
	Kelurahan    int64    `json:"kelurahan"`
	TypeMerchant string `json:"type_merchant"`
}

type GenMidResponse struct {
	Mid 	string `json:"MID"`
}