package db

import (
	"belajargin/db/dbmodels"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"log"
)

func (database *DbPostgres) GetAllCustomer() ([]dbmodels.Customer, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : GetAllCustomer",
		zap.Any("req", ""))
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : GetAllCustomer")
	defer span.Finish()

	var res []dbmodels.Customer
	err := DbCon.Find(&res).Error

	if err != nil {
		log.Println("Failed to get Customer : ", err)
		sugarLogger.Error("Failed to get Customer : ", err)
		return res, err
	}

	sugarLogger.Info("Success get Customer : ", res)
	return res, nil
}

func (database *DbPostgres) GetCustomerById(id int64) ([]dbmodels.Customer, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : GetCustomerById",
		zap.Any("req", id))
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : GetCustomerById")
	defer span.Finish()

	var res []dbmodels.Customer
	err := DbCon.Where("ID = ?", id).Find(&res).Error

	if err != nil {
		log.Println("Failed to get Customer : ", err)
		sugarLogger.Error("Failed to get Customer : ", err)
		return res, err
	}

	sugarLogger.Info("Success get Customer : ", res)
	return res, nil
}

func (database *DbPostgres) AddNewCustomer(res dbmodels.Customer) (string, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : AddCustomer")
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : Add Customer")
	defer span.Finish()

	err := DbCon.Create(&res).Error

	if err != nil {
		log.Println("Failed to Insert : ", err)
		sugarLogger.Error("Failed to Insert :", err)
		return "false", err
	}
	sugarLogger.Info("Success Insert  :", res)
	return "ok", nil
}

func (database *DbPostgres) UpdateCustomer(res dbmodels.Customer, id int64) (string, error) {

	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : UpdateCustomer")

	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : Update Customer")
	defer span.Finish()

	err := DbCon.Model(&res).Where("id =?", id).Update(&res).Error

	if err != nil {
		log.Println("Failed to Update : ", err)
		sugarLogger.Error("Failed to Update", err)
		return "false", err
	}
	sugarLogger.Info("Success Update Customer", res)
	return "OK", nil
}

func (database *DbPostgres) DeleteCustomer(id int64) (string, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : UpdateBarang")
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : Delete Customer")
	defer span.Finish()

	var res []dbmodels.Customer

	err := DbCon.Where("id =?", id).Delete(&res).Error
	if err != nil {
		log.Println("Failed to Delete :", err)
		sugarLogger.Error("Failed to Delete", err)
		return "false", err
	}
	sugarLogger.Info("Success Delete Customer", res)
	return "ok", nil
}
