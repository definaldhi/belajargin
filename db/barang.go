package db

import (
	"belajargin/db/dbmodels"
	"github.com/opentracing/opentracing-go"
	"go.uber.org/zap"
	"log"
)

func (database *DbPostgres) GetAllBarang() ([]dbmodels.Barang, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : GetAllBarang",
		zap.Any("req", ""))
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : GetAllBarang")
	defer span.Finish()

	var res []dbmodels.Barang
	err := DbCon.Find(&res).Error

	if err != nil {
		log.Println("Failed to get Barang : ", err)
		sugarLogger.Error("Failed to get Barang : ", err)
		return res, err
	}

	sugarLogger.Info("Success get Barang : ", res)
	return res, nil
}

func (database *DbPostgres) GetBarangById(id int64) ([]dbmodels.Barang, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : GetBarangById",
		zap.Any("req", id))
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : GetBarangById")
	defer span.Finish()

	var res []dbmodels.Barang
	err := DbCon.Where("ID = ?", id).Find(&res).Error

	if err != nil {
		log.Println("Failed to get Users : ", err)
		sugarLogger.Error("Failed to get Users : ", err)
		return res, err
	}

	sugarLogger.Info("Success get Barang : ", res)
	return res, nil
}

func (database *DbPostgres) AddNewBarang(res dbmodels.Barang) (string, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : AddBarang")
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : Add Barang")
	defer span.Finish()

	err := DbCon.Create(&res).Error

	if err != nil {
		log.Println("Failed to Insert : ", err)
		sugarLogger.Error("Failed to Insert :", err)
		return "false", err
	}
	sugarLogger.Info("Success Insert Barang :", res)
	return "ok", nil
}

func (database *DbPostgres) UpdateBarang(res dbmodels.Barang, id int64) (string, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : UpdateBarang")
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : Update Barang")
	defer span.Finish()

	err := DbCon.Model(&res).Where("id =?", id).Update(&res).Error

	if err != nil {
		log.Println("Failed to Update : ", err)
		sugarLogger.Error("Failed to Update", err)
		return "false", err
	}
	sugarLogger.Info("Success Update User", res)
	return "ok", nil
}

func (database *DbPostgres) DeleteBarang(id int64) (string, error) {
	sugarLogger := database.General.OttoZapLog
	sugarLogger.Info("DB : UpdateBarang")
	span, _ := opentracing.StartSpanFromContext(database.General.Context, "DB : Delete Barang")
	defer span.Finish()

	var res []dbmodels.Barang

	err := DbCon.Where("id =?", id).Delete(&res).Error
	if err != nil {
		log.Println("Failed to Delete :", err)
		sugarLogger.Error("Failed to Delete", err)
		return "false", err
	}
	sugarLogger.Info("Success Delete User", res)
	return "ok", nil
}
