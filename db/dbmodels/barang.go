package dbmodels

type Barang struct {
	ID          int64  `json:"id"`
	NamaBarang  string `json:"nama_barang"`
	HargaBarang string `json:"harga_barang"`
	Detail      string `json:"detail"`
}

func (t *Barang) TableName() string {
	return "barang"
}

type Customer struct {
	ID           int64  `json:"id"`
	CustomerName string `json:"customer_name"`
	Address      string `json:"address"`
	Username     string `json:"username"`
	Password     string `json:"password"`
}

func (t *Customer) TableName() string {
	return "customer"
}
