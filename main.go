package main

import (
	"belajargin/routers"
	"fmt"
	"github.com/uber/jaeger-lib/metrics"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"ottodigital.id/library/logger"
	"ottodigital.id/library/utils"
	"runtime"
	"strconv"
	"syscall"
)

var (
	metricsBackend string
	metricsFactory metrics.Factory
	listenAddrApi  string
	sugarLogger    *zap.SugaredLogger
)

func main() {

	// init Logger Zap with file
	sugarLogger := logger.GetLogger()
	defer sugarLogger.Sync()

	maxProc, _ := strconv.Atoi(utils.GetEnv("MAX_PROC", "1"))

	runtime.GOMAXPROCS(maxProc)

	var errChan = make(chan error, 1)

	go func() {
		fmt.Println("Starting")
		sugarLogger.Info("Starting ", zap.String("Start", "here"))
		errChan <- routers.Server(utils.GetEnv("OTTOPOINT_BACKEND_API_SERVER_ADDRESS", "0.0.0.0:8701"))
	}()

	var signalChan = make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)
	select {
	case <-signalChan:
		fmt.Println("Got an interrupt, exiting...")
		sugarLogger.Error("Got an interrupt, exiting...")
	case err := <-errChan:
		if err != nil {
			fmt.Println("Error while running api, exiting:", err)
			sugarLogger.Error("Error while running api, exiting... ", zap.Error(err))
		}
	}

}
